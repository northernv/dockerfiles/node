> Custom Alpine Node Images

## Versions

* Node 12 Alpine: `12-alpine`
* Node 14 Alpine: `14-alpine`
* Node 16 Alpine: `16-alpine`
* Node 18 Alpine: `18-alpine`

## New Builds are triggered weekly by Gitlab


## Usage 

```
docker pull registry.gitlab.com/northernv/dockerfiles/node:16-alpine
```
